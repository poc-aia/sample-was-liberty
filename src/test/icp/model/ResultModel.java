package test.icp.model;

public class ResultModel {
	private static final String app = "TestICP";
	private String message;

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public static String getApp() {
		return app;
	}
}
