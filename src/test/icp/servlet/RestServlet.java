package test.icp.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import test.icp.rest.provider.model.RestResultModel;
import test.icp.rest.provider.service.RestServiceI;
import test.icp.util.FormatFactory;
import test.icp.util.HttpLogUtil;
import test.icp.util.ObjectUtil;
import test.icp.util.StringUtil;

import com.ibm.json.java.JSONObject;

public class RestServlet extends HttpServlet {
	private static final Logger log = Logger.getLogger(RestServlet.class);

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		RestResultModel restResult = new RestResultModel();
		FormatFactory format = null;
		log.info("["+restResult.getTrxCode()+"] start");
		try {
			//find process class by uri
			String requestUri = request.getRequestURI().replace(request.getContextPath(), "");
			RestServiceI restService = findJavaClassByRequestPath(requestUri);
			if(restService==null){
				throw new Exception("Not found restService for requestUri = "+requestUri);
			}
			log.info("["+restResult.getTrxCode()+"] found restService class = "+restService.getClass().getName()+" for requestUri = "+requestUri);
			
			//parse json to object
			String json = request.getParameter("json");
			Class inputClass = restService.getInputClass();
			Object input = null;
			if(inputClass!=null){
				if(StringUtil.isBlank(json)){
					throw new Exception("Please input json");
				}
				log.info("["+restResult.getTrxCode()+"] parse to input class = "+inputClass.getClass().getName()+" with json = "+json);
				input = ObjectUtil.fromJson(JSONObject.parse(json), FormatFactory.restProviderFormat, inputClass);
			}
			
			//process class
			restResult.setupServiceInfo(restService, json, request);
			Object result = restService.execute(input);
			
			//set response
			restResult.responseSuccess(result);
			format = restService.getFormatFactor();
			
			log.info("["+restResult.getTrxCode()+"] end");
		} catch (Exception e) {
			log.error("["+restResult.getTrxCode()+"] error : "+e.getMessage(), e);
			restResult.responseError(e);
		}
		responseJson(restResult, format, response);
	}
	
	private RestServiceI findJavaClassByRequestPath(String path) {
		try{
			path = path.replace("/", ".");
			path = path.replace(".rest", "test.icp.rest.provider.service");
			String javaClass = path;
			Class clz = Class.forName( javaClass );
			Object obj = clz.newInstance();
			if(!(obj instanceof RestServiceI)){
				log.error("error to find class with path["+path+"] : obj must be instance of "+RestServiceI.class.getName());
				return null;
			}
			return (RestServiceI) obj;
		} catch (Exception e) {
			log.error("error to find class with path["+path+"] : "+e.getMessage(), e);
			return null;
		}
	}
	
	private void responseJson(RestResultModel result, FormatFactory format, HttpServletResponse response){
		try{
			if(format==null){
				format = FormatFactory.restProviderFormat;
			}
			
			//prase json
			String json = ObjectUtil.toJson(result, format).serialize();
			log.info("["+result.getTrxCode()+"] responseJson = "+json);
			HttpLogUtil.log( json );
			
			//write response
			response.setHeader("Cache-Control","private, max-age=1");
			response.setHeader("Pragma",""); 
			response.setHeader("Content-Disposition","inline; filename=result.json");
			response.setDateHeader("Expires", -1); 
			response.getOutputStream().write(json.getBytes());
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (Exception e) {
			log.error("error : "+e.getMessage(), e);
		}
	}
	
}
