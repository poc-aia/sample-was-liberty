package test.icp.util;

import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;

public class NumberUtil {
	public static final DecimalFormat bigDecimalFormat = new DecimalFormat("####.####################");
	public static final DecimalFormat integerFormat = new DecimalFormat("###0");
	public static final DecimalFormat decimalFormat = new DecimalFormat("###0.00");
	public static final DecimalFormat currencyIntFormat = new DecimalFormat("#,##0");
	public static final DecimalFormat currencyFormat = new DecimalFormat("#,##0.00");

	public static Number toNumber(Object obj, Class clz){
		if(clz==null){
			return null;
		}
		try{
			Class objClz = clz.asSubclass(Number.class);
			if(objClz==null){
				return null;
			}
			String str = obj.toString().replaceAll(",", "").trim();
			Constructor constructor = objClz.getConstructor(String.class);
			Number number = (Number) constructor.newInstance(str);
			return number;
		} catch(Exception e){
			return null;
		}
	}
	
	public static Integer toInteger(Object obj){
		if(obj==null) return 0;

		try{
			if(obj instanceof Number){
				if(obj instanceof BigDecimal){
					return ((BigDecimal) obj).intValue();
				} else if(obj instanceof Double){
					return ((Double) obj).intValue();
				} else if(obj instanceof Integer){
					return (Integer) obj;
				} else if(obj instanceof Long){
					return ((Long) obj).intValue();
				} else {
					Number num = (Number) obj;
					return new Integer(decimalFormat.format(num));
				}
			}
			BigDecimal num = new BigDecimal(obj.toString().trim().replace(",", ""));
			return num.intValue();
		} catch(Exception e){
			return 0;
		}
	}

	public static Double toDouble(Object obj){
		if(obj==null) return 0.0;

		try{
			if(obj instanceof Number){
				if(obj instanceof BigDecimal){
					return ((BigDecimal) obj).doubleValue();
				} else if(obj instanceof Double){
					return ((Double) obj).doubleValue();
				} else if(obj instanceof Integer){
					return (Double) obj;
				} else if(obj instanceof Long){
					return ((Double) obj).doubleValue();
				} else {
					Number num = (Number) obj;
					return new Double(decimalFormat.format(num));
				}
			}
			BigDecimal num = new BigDecimal(obj.toString().trim().replace(",", ""));
			return num.doubleValue();
		} catch(Exception e){
			return 0.0;
		}
	}
	
	public static BigDecimal toBigDecimal(Object obj){
		if(obj==null) return BigDecimal.ZERO;

		try{
			if(obj instanceof Number){
				if(obj instanceof BigDecimal){
					return (BigDecimal) obj;
				} else if(obj instanceof Double){
					return new BigDecimal((Double) obj);
				} else if(obj instanceof Integer){
					return new BigDecimal((Integer) obj);
				} else if(obj instanceof Long){
					return new BigDecimal((Long) obj);
				} else {
					Number num = (Number) obj;
					return new BigDecimal( decimalFormat.format(num) );
				}
			}
			BigDecimal num = new BigDecimal(obj.toString().trim().replace(",", ""));
			return num;
		} catch(Exception e){
			return BigDecimal.ZERO;
		}
	}
	
	public static boolean isNumber(Object obj){
		if(obj==null){
			return false;
		}
		
		if(obj instanceof Number){
			return true;
		}
		
		try{
			Double.parseDouble(obj.toString().trim().replace(",", ""));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String formatByType(Number val) {
		if(val==null) val = 0;
		
		if(val instanceof Integer){
			return integerFormat.format(val);
		} else if(val instanceof Long){
			return integerFormat.format(val);
		} else if(val instanceof Short){
			return integerFormat.format(val);
		} else if(val instanceof BigInteger){
			return integerFormat.format(val);
		} else {
			return decimalFormat.format(val);
		}
	}
	
	public static String formatCurrency(Number val) {
		if(val==null) val = 0;
		return currencyFormat.format(val);
	}
	
	private static String numberInThaiBath(char no, int position) {
		switch(no) {
		case '1':
			if(position == 2)
				return "";
			return "ห�?ึ�?�?";
		case '2':
			if(position == 2)
				return "ยี�?";
			return "สอ�?";
		case '3':
			return "สาม";
		case '4':
			return "สี�?";
		case '5':
			return "ห�?า";
		case '6':
			return "ห�?";
		case '7':
			return "เ�?�?�?";
		case '8':
			return "�?�?�?";
		case '9':
			return "เ�?�?า";
		}
		return "";
	}
	
	private static String fontInThaiBath(int i) {
		switch(i) {
			case 7:
				return "ล�?า�?";
			case 6:
				return "�?ส�?";
			case 5:
				return "หมื�?�?";
			case 4:
				return "�?ั�?";
			case 3:
				return "ร�?อย";
			case 2:
				return "สิ�?";
		}
		return "";
	}
	
	public static String toThaiBath(Number val){
		if(val==null) val = 0;
		if(val.doubleValue()==0){
			return "ศู�?ย�?�?า�?";
		}

		StringBuilder numberThai = new StringBuilder();
		String number = decimalFormat.format(val);
		if(number != null && !number.equals("")) {
			String[] numberArr = number.split("[.]");
			if(Integer.parseInt(numberArr[0])>0){
				int leftTot = numberArr[0].length();
				if(leftTot > 6) {
					int left2 = leftTot-6;
					String leftStr2 = numberArr[0].substring(0, left2);
					for(int i=left2; i>0; i--) {
						char c = leftStr2.charAt(left2-i);
						if(c != '0') {
							numberThai.append(numberInThaiBath(c, i));
							numberThai.append(fontInThaiBath(i));
						}
					}
					numberThai.append("ล�?า�?");
					String leftStr = numberArr[0].substring(left2);
					int left = leftStr.length();
					for(int i=left; i>0; i--) {
						char c = leftStr.charAt(left-i);
						if(c != '0') {
							numberThai.append(numberInThaiBath(c, i));
							numberThai.append(fontInThaiBath(i));
						}
					}
				} else {
					String leftStr = numberArr[0];
					for(int i=leftTot; i>0; i--) {
						char c = leftStr.charAt(leftTot-i);
						if(c != '0') {
							numberThai.append(numberInThaiBath(c, i));
							numberThai.append(fontInThaiBath(i));
						}
					}
				}
				numberThai.append("�?า�?");
			}
			if(numberArr.length>=2 && numberArr[1].length()<=2 && Integer.parseInt(numberArr[1])>0) {
				String rightStr = numberArr[1];
				if(rightStr.length()==1){
					rightStr = rightStr+"0";
				}
				int rightTot = rightStr.length();
				for(int i=rightTot; i>0; i--) {
					char c = rightStr.charAt(rightTot-i);
					if(c != '0') {
						numberThai.append(numberInThaiBath(c, i));
						numberThai.append(fontInThaiBath(i));
					}
				}
				numberThai.append("ส�?า�?�?�?");
			} else {
				numberThai.append("�?�?ว�?");
			}
		}
		String numberStr = numberThai.toString();
		numberStr = numberStr.replace("สิ�?ห�?ึ�?�?", "สิ�?เอ�?�?");
		return numberStr;
	}
}
