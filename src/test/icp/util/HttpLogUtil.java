package test.icp.util;

public class HttpLogUtil {
	private static final HttpConnection conn;
	
	static {
		conn = new HttpConnection("", "", null) {
			protected String getContextRoot() {
				return "";
			}
		};
	}
	
	public static void log(String msg) throws Exception {
		conn.submitPost("http://10.141.111.121:30144", msg);
	}
}
