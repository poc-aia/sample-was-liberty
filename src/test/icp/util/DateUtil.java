package test.icp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class DateUtil {
	private static final Logger log = Logger.getLogger(DateUtil.class);
	private static final Map<Integer, String> thaiMonthMap;
	public static final String defaultDateFormat = "MM-dd-yyyy";
	public static final String defaultDateTimeFormat = "MM-dd-yyyy HH:mm:ss";
	public static final String compassDateFormat = "MM/dd/yyyy";
	public static final String igmDateFormat = "yyyy-MM-dd";
	public static final String clientLogDateFormat = "MM-dd-yyyy HH:mm:ss";
	public static final Locale defaultLocale = Locale.US;
	public static final Locale localeTh = new Locale("th", "TH");
	
	static {
		thaiMonthMap = new HashMap<Integer, String>();
		thaiMonthMap.put(1, "ม.ค.");
		thaiMonthMap.put(2, "ก.พ.");
		thaiMonthMap.put(3, "มี.ค.");
		thaiMonthMap.put(4, "เม.ย.");
		thaiMonthMap.put(5, "พ.ค.");
		thaiMonthMap.put(6, "มิ.ย.");
		thaiMonthMap.put(7, "ก.ค.");
		thaiMonthMap.put(8, "ส.ค.");
		thaiMonthMap.put(9, "ก.ย.");
		thaiMonthMap.put(10, "ต.ค.");
		thaiMonthMap.put(11, "พ.ย.");
		thaiMonthMap.put(12, "ธ.ค.");
	}
	
	public static Date getCurrentTime() {
		return Calendar.getInstance(defaultLocale).getTime();
	}
	
	public static Date getCurrentDate() {
		Date now = Calendar.getInstance(defaultLocale).getTime();
		now = parseDate( formatDate(now) );
		return now;
	}
	
	public static String formatThaiDate(Date dte) {
		if(dte==null){
			return null;
		}
		
		String dd = DateUtil.formatDate(dte, "dd");
		String mm = DateUtil.getMonthInTh( Integer.parseInt(DateUtil.formatDate(dte, "MM")) );
		String yy = DateUtil.formatDate(dte, "yyyy", new Locale("TH"));
		return dd+" "+mm+" "+yy;
	}
	
	public static String formatDate(Date dte) {
		return formatDate(dte, defaultDateFormat, defaultLocale);
	}
	
	public static String formatDate(Date dte, String format) {
		return formatDate(dte, format, defaultLocale);
	}
	
	public static String formatDate(Date dte, String format, Locale locale) {
		DateFormat df = new SimpleDateFormat(format, locale);
		return df.format(dte);
	}
	
	public static Date parseDate(String dateStr) {
		return parseDate(dateStr, defaultDateFormat, defaultLocale);
	}

	public static Date parseDate(String dateStr, String format) {
		return parseDate(dateStr, format, defaultLocale);
	}
		
	public static Date parseDate(String dateStr, String format, Locale locale) {
		DateFormat df = new SimpleDateFormat(format, locale);
		try {
			return df.parse(dateStr);
		} catch (Exception e) {
			return null;
		}
	}

	public static Date toDate(Object obj) throws Exception {
		if(obj==null){
			return null;
		} else if(obj instanceof Date){
			return (Date) obj;
		} else if(obj instanceof Number){
			Number num = (Number) obj;
			return new Date(num.longValue());
		} else if(obj instanceof String){
			return DateUtil.parseDate(obj.toString());
		} else if(obj instanceof java.sql.Timestamp){
			return new Date(((java.sql.Timestamp) obj).getTime());
		} else {
			throw new Exception("error : Not found toDate for class "+obj.getClass().getName());
		}
	}

	public static Date toDate(Object obj, String format, Locale locale) {
		if(obj==null){
			return null;
		} else if(obj instanceof Date){
			return (Date) obj;
		} else if(obj instanceof Calendar){
			return ((Calendar) obj).getTime();
		} else if(obj instanceof Number){
			Number num = (Number) obj;
			return new Date(num.longValue());
		} else {
			return DateUtil.parseDate(obj.toString(), format, locale);
		}
	}
	public static Date addDate(Date d, int timeType, int i) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(timeType, i);
		return c.getTime();
	}
	
	public static Date goToDate(Date d, int timeType, int i) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(timeType, i);
		return c.getTime();
	}
	
	public static String getMonthInTh(Date d) {
		if(d==null) return null;
		int month = Integer.parseInt(formatDate(d, "MM"));
		return getMonthInTh(month);
	}
	
	public static long dateDiff(Date date1, Date date2, int timeType){
		Date dateMax = null;
		Date dateMin = null;
		if(date1.getTime()<date2.getTime()){
			dateMax = date2;
			dateMin = date1;
		} else {
			dateMax = date1;
			dateMin = date2;
		}
		long diffTime = dateMax.getTime() - dateMin.getTime();

		if(Calendar.MILLISECOND==timeType){
			return diffTime;
		} else if(Calendar.SECOND==timeType){
			return TimeUnit.MILLISECONDS.toSeconds(diffTime);
		} else if(Calendar.MINUTE==timeType){
			return TimeUnit.MILLISECONDS.toMinutes(diffTime);
		} else if(Calendar.HOUR==timeType){
			return TimeUnit.MILLISECONDS.toHours(diffTime);
		} else if(Calendar.DATE==timeType){
			return TimeUnit.MILLISECONDS.toDays(diffTime);
		} else if(
				Calendar.MONTH==timeType
				|| Calendar.YEAR==timeType
			){
			Calendar calMax = Calendar.getInstance();
			calMax.clear();
			calMax.setTime( parseDate(formatDate(dateMax)) );
			
			Calendar calMin = Calendar.getInstance();
			calMin.clear();
			calMin.setTime( parseDate(formatDate(dateMin)) );
			
			int diff = -1;
			while(calMax.after(calMin) || calMax.equals(calMin)) {
				calMin.add(timeType, 1);
				diff++;
			}
			return diff;
		} else {
			return diffTime;
		}
	}
	
	public static String getMonthInTh(int m) {
		return thaiMonthMap.get(m);
	}
	
	public static Calendar timestampToCalendar(java.sql.Timestamp t){
		if(t==null) return null;
		
		Calendar c = Calendar.getInstance();
		
		c.setTimeInMillis(t.getTime());
		
		return c;
	}
	
	public static long compareCalendar(Calendar c1,Calendar c2,int returnType){
		long result = 0;
		
		long seconds = (c2.getTimeInMillis()-c1.getTimeInMillis())/1000;
		int minutes = (int)seconds/60;
		int hours = (int)minutes/60;
		
		if(returnType==13){
			result = seconds;
		}else if(returnType==12){
			result = minutes;
		}else if(returnType==10){
			result = hours;
		}
		
		return result;
	}
}
