package test.icp.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

public abstract class HttpConnection {
	private static final Logger log = Logger.getLogger(HttpConnection.class);
	public static final Integer defaultTimeout = 30000;
	public static final String requestMethodGet = "GET";
	public static final String requestMethodPost = "POST";
	private Map<String, String> requestProperties = null;
	
	protected HttpConnection(
			String username, String password, Map<String, String> requestProp
		) {
		
		this.requestProperties = new HashMap<String, String>();
		if(requestProp!=null){
			requestProperties.putAll(requestProp);
		}
//		if(!StringUtil.isBlank(username) && !StringUtil.isBlank(password)){
//			BASE64Encoder encoder = new Base64encod();
//			String authStr = username+":"+password;
//			requestProperties.put("Authorization", "Basic "+encoder.encode(authStr.getBytes()));
//		}
	}
	
	protected abstract String getContextRoot();
	
	public String submitGet(String url) throws Exception {
		return submit(url, "", requestMethodGet, defaultTimeout, null);
	}
	
	public String submitPost(String url, Object param) throws Exception {
		return submit(url, param, requestMethodPost, defaultTimeout, null);
	}
	
	public String submit(
			String url, Object param, String requestMethod
			, Integer timeoutMs, Map<String, String> requestProp
		) throws Exception {
		
		url = getContextRoot()+url;
		log.info("start : url = "+url);
		try{
			//prepare resource
			URL urlObj = new URL(url);
			Map<String, String> reqProp = new HashMap<String, String>();
			reqProp.putAll(requestProperties);
			if(requestProp!=null){
				reqProp.putAll(requestProp);
			}
			
			HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
			//timeout
			conn.setConnectTimeout(5000);
			if(timeoutMs!=null && timeoutMs>0){
				conn.setReadTimeout(timeoutMs);
			} else {
				conn.setReadTimeout(defaultTimeout);
			}
			
			//request method
			if(StringUtil.isBlank(requestMethod)){
				requestMethod = requestMethodPost;
			}
			conn.setRequestMethod(requestMethod.toUpperCase());
			
			//request property
			Iterator<String> itr = reqProp.keySet().iterator();
			while(itr.hasNext()){
				String key = itr.next();
				String val = reqProp.get(key);
				conn.setRequestProperty(key, val);
			}
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			//parameter
			String payload = toPayload(param);
			if(!StringUtil.isBlank(payload)){
				log.info("payload = "+payload);
				OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
				writer.write(payload);
				writer.flush();
				writer.close();
			}
			
			//check status
			int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK) {
				throw new Exception("fail to execute service : httpStatus = "+status+", message = "+conn.getResponseMessage());
			}
			
			//build output string
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			StringBuffer buffer = new StringBuffer();
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer.append(line).append("\n");
			}
			reader.close();
			conn.disconnect();
			
			String result = buffer.toString();
			log.info("end : result length = "+result.length());
			return result;
		} catch (Exception e) {
			log.error("error : "+e.getMessage(), e);
			throw e;
		}
	}
	
	private String toPayload(Object obj) throws Exception {
		if(obj==null){
			return null;
		}
		
		if(obj instanceof Map){
			Map param = (Map) obj;
	        StringBuffer requestStringBuffer = new StringBuffer();
	        Iterator itr = param.keySet().iterator();
	        for(int i=0; itr.hasNext(); i++) {
	            Object key = itr.next();
	            Object val = param.get(key);
	            if(val==null){
	            	continue;
	            }
	            requestStringBuffer.append("&"+key+"="+URLEncoder.encode(val.toString(), "UTF-8"));
	        }
	        String paramStr = requestStringBuffer.toString();
	        if(paramStr.indexOf("&")==0){
	        	paramStr = paramStr.substring(1);
	        }
	        return paramStr;
		}
		return obj.toString();
	}
}
