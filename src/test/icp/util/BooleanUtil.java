package test.icp.util;

public class BooleanUtil {
	public static Boolean toBoolean(Object obj){
		if(obj==null) return false;

		try{
			if(obj instanceof Boolean){
				return (Boolean) obj;
			}
			if(obj instanceof String){
				String str = obj.toString().trim();
				if(str.equalsIgnoreCase("Y")){
					return true;
				} else if(str.equalsIgnoreCase("Yes")){
					return true;
				} else if(str.equalsIgnoreCase("T")){
					return true;
				} else if(str.equalsIgnoreCase("true")){
					return true;
				} else {
					return false;
				}
			}
			if(obj instanceof Number){
				return ((Number) obj).intValue()>0?true:false;
			}
			return false;
		} catch(Exception e){
			return false;
		}
	}

}
