package test.icp.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ibm.json.java.JSONArray;
import com.ibm.json.java.JSONArtifact;
import com.ibm.json.java.JSONObject;

public class ObjectUtil {
	private static final Logger log = Logger.getLogger(ObjectUtil.class);
	
	public static Object newInstance(String clzName, Object... constructObj) throws Exception {
		Class clz = Class.forName(clzName);
		Object obj = null;
		if(constructObj==null){
			obj = clz.newInstance();
		} else {
			Constructor construcList[] = clz.getConstructors();
			for(int i=0; i<construcList.length; i++){
				Class constructClz[] = construcList[i].getParameterTypes();
				if(constructClz==null){
					continue;
				}
				if(constructClz.length!=constructObj.length){
					continue;
				}
				boolean isFound = true;
				for(int j=0; j<constructClz.length; j++){
					if(!constructClz[j].isInstance(constructObj[j])){
						isFound = false;
						break;
					}
				}
				if(isFound){
					obj = construcList[i].newInstance(constructObj);
				}
			}
		}
		return obj;
	}
	
	public static Map<String, String> parseArrToMap(String header[], String detail[]) {
		Map<String, String> resultMap = new HashMap<String, String>();
		if(header==null || detail==null){
			return resultMap;
		}
		if(header.length!=detail.length){
			return resultMap;
		}
		
		for(int i=0; i<header.length; i++){
			if(header[i]==null || detail[i]==null){
				continue;
			}
			resultMap.put(header[i].trim(), detail[i].trim());
		}
		
		return resultMap;
	}
	
	public static String printAllAttribute(Object obj) {
		StringBuffer sb = new StringBuffer();
		try {
			if(obj==null) return "Object is null";

			if(obj instanceof List){
				List list = (List) obj;
				sb.append("List of [");
				for(int i=0; i<list.size(); i++){
					try{
						sb.append("|"+i+"| "+printAllAttribute(list.get(i))+" ");
					} catch(Exception e){
						sb.append("Exception:"+e.getMessage());
					}
				}
				sb.append("]");
				return sb.toString();
			} else if(obj instanceof Object[]){
				Object list[] = (Object[]) obj;
				sb.append("Array of [");
				for(int i=0; i<list.length; i++){
					try{
						if(list[i] instanceof Comparable){
							sb.append("|"+i+"| "+printAllAttribute(list[i])+" ");
						} else {
							sb.append("{"+printAllAttribute(list[i])+"}");
						}
						if(i<list.length-1){
							sb.append(",");
						}
					} catch(Exception e){
						sb.append("Exception:"+e.getMessage());
					}
				}
				sb.append("]");
				return sb.toString();
			} else if(obj instanceof Map){
				Map map = (Map) obj;
				Iterator itr = map.keySet().iterator();
				sb.append("Map of [");
				sb.append("{");
				while(itr.hasNext()){
					Object key = itr.next();
					Object val = map.get(key);
					sb.append(key+":");
					try{
						sb.append( printAllAttribute(val) );
					} catch(Exception e){
						sb.append("Exception:"+e.getMessage());
					}
				}
				sb.append("}");
				return sb.toString();
			}
			
			Class clz = obj.getClass();
			while(!clz.equals(Object.class)){
				Field field[] = clz.getDeclaredFields();
				for (int i = 0; i < field.length; i++) {
					try {
						String fieldName = field[i].getName();
						Object val = getFieldValue(obj, field[i]);
						if(val==null || val instanceof Comparable){
							sb.append(", "+fieldName+" = "+val);
						} else {
							String str = printAllAttribute(val);
							sb.append(", "+fieldName+" is ("+str+")");
						}
					} catch (Exception e) {
					}
				}
				
				clz = clz.getSuperclass();
			}
		} catch (Exception e) {
		}
		
		String str = sb.toString();
		str = str.indexOf(", ")==0?str.substring(", ".length()):str;
		return str;
	}
	
	public static void clearObj(Object obj) {
		try {
			if(obj==null) return;
			
			Class clz = obj.getClass();
			while(!clz.equals(Object.class)){
				Field field[] = clz.getDeclaredFields();
				for (int i = 0; i < field.length; i++) {
					try {
						String fieldName = field[i].getName();
						String mthName = "set" + fieldName.substring(0, 1).toUpperCase() + "" + fieldName.substring(1);
						Method mth = clz.getMethod(mthName, field[i].getType() );
						Object val = mth.invoke(obj, new Object[]{ null } );
					} catch (Exception e) {
					}
				}
				clz = clz.getSuperclass();
			}
		} catch (Exception e) {
		}
	}

	public static void trimAll(Object obj) {
		try {
			if(obj==null) return;

			Class clz = obj.getClass();
			while(!clz.equals(Object.class)){
				Field field[] = clz.getDeclaredFields();
				for (int i = 0; i < field.length; i++) {
					try {
						String fieldName = field[i].getName();
						Object val = getFieldValue(obj, field[i]);
						if(val!=null){
							if(val instanceof String){
								Method setMth = obj.getClass().getMethod("set" + fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1), String.class);
								setMth.invoke(obj, ((String) val).trim());
							} else {
								trimAll(val);
							}
						}
					} catch (Exception e) {
					}
				}
				
				clz = clz.getSuperclass();
			}
		} catch (Exception e) {
		}
	}

	public static void cloneObj(Object fromObj, Object toObj) {
		try {
			if(fromObj==null || toObj==null) return;

			Class clz = fromObj.getClass();
			while(!clz.equals(Object.class)){
				Field field[] = clz.getDeclaredFields();
				for (int i = 0; i < field.length; i++) {
					try {
						String fieldName = field[i].getName();
						Object val = getFieldValue(fromObj, field[i]);
						if(val!=null){
							if(val instanceof String){
								Method setMth = toObj.getClass().getMethod("set" + fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1), String.class);
								setMth.invoke(toObj, ((String) val).trim());
							} else {
								trimAll(val);
							}
							Method setMth = null;
							String objType = field[i].getType().getName();
							Class attrClz = null;
							try{
								attrClz = Class.forName(field[i].getType().getName());
							} catch (Exception e) {
								if("boolean".equalsIgnoreCase(field[i].getType().getName())){
									attrClz = Boolean.class;
								}
							}
							if(attrClz==null){
								continue;
							}
							
							if("boolean".equalsIgnoreCase(objType) || Boolean.class.getName().equalsIgnoreCase(objType) ){
								if(fieldName.indexOf("is")==0){
									String mthName = fieldName.substring(2, 3).toUpperCase() + fieldName.substring(3);
									setMth = toObj.getClass().getMethod("set" + mthName, attrClz);
								} else {
									String mthName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
									setMth = toObj.getClass().getMethod("set" + mthName, attrClz);
								}
							} else {
								String mthName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
								setMth = toObj.getClass().getMethod("set" + mthName, attrClz);
							}
							if(setMth==null){
								continue;
							}
							setMth.invoke(toObj, val);
						}
					} catch (Exception e) {
					}
				}
				
				clz = clz.getSuperclass();
			}
		} catch (Exception e) {
		}
	}

	public static void copyAttr(Object source, Object desc){
		if(source==null || desc==null) return;
		Method m[] = source.getClass().getMethods();
		for(int i=0; i<m.length; i++){
			try{
				if(m[i].getName().toLowerCase().indexOf("get")==0){
					Object result = m[i].invoke(source, null);
					if(result==null) continue;
					
					String setMethodName = "set"+m[i].getName().substring("get".length());
					Method setMethod = desc.getClass().getMethod(setMethodName, m[i].getReturnType());
					setMethod.invoke(desc, result);
				}
			} catch (Exception e) {
			}
		}
	}

	public static Object fromJson(JSONArtifact json, FormatFactory format, Class clz, Class... genericClzArr) {
		try {
			if(json==null){
				return null;
			}
			if(clz==null){
				return null;
			}
			
			//build Array from json
			if(json instanceof JSONArray){
				List list = new ArrayList();
				JSONArray jsonArray = (JSONArray) json;
				for(int i=0; i<jsonArray.size(); i++){
					Object o = jsonArray.get(i);
					if(o instanceof JSONArtifact){
						JSONArtifact jsonObj = (JSONArtifact) o;
						Object obj = fromJson(jsonObj, format, clz, genericClzArr);
						list.add(obj);
					} else if(o instanceof String){
						String str = (String) o;
						if(StringUtil.isBlank(str)){
							continue;
						}
						list.add(str);
					} else {
						list.add(o);
					}
				}
				return list;
			}
			//build Object from json
			else if(json instanceof JSONObject){
				JSONObject jsonObj = (JSONObject) json;
				Object obj = clz.newInstance();
				if(obj instanceof Map){
					Map map = (Map) obj;
					Iterator itr = jsonObj.keySet().iterator();
					Class genericClz1 = null;
					Class genericClz2[] = null;
					if(genericClzArr==null || genericClzArr.length==0){
						genericClz1 = String.class;
					} else {
						genericClz1 = genericClzArr[0];
						genericClz2 = new Class[genericClzArr.length-1];
						for(int i=1; i<genericClzArr.length; i++){
							genericClz2[i-1] = genericClzArr[i];
						}
					}
					while(itr.hasNext()){
						Object key = itr.next();
						Object val = jsonObj.get(key);
						if(val==null){
							continue;
						} else if(val instanceof JSONArtifact){
							JSONArtifact jsonArt = (JSONArtifact) val;
							Object objArt = fromJson(jsonArt, format, genericClz1, genericClz2);
							map.put(key, objArt);
						} else {
							map.put(key, val);
						}
					}
					return map;
				} else {
					Class objClz = obj.getClass();
					while(!objClz.equals(Object.class)){
						Field field[] = objClz.getDeclaredFields();
						for (int i = 0; i < field.length; i++) {
							try {
								String fieldName = field[i].getName();
								Object val = jsonObj.get(fieldName);
								if(val==null){
									continue;
								} else if(val instanceof JSONArtifact){
									JSONArtifact jsonArt = (JSONArtifact) val;
									Type type = field[i].getGenericType();
						            if (type instanceof ParameterizedType) {
						                ParameterizedType pType = (ParameterizedType)type;
						                Type[] arr = pType.getActualTypeArguments();
					                    if(jsonArt instanceof JSONArray){
						                    Class<?> genericClzz = (Class<?>)arr[0];
						                    val = fromJson(jsonArt, format, genericClzz, genericClzz);
					                    } else {
					                    	Class mapClz = field[i].getType();
					                    	if(field[i].getType().isInterface()){
					                    		mapClz = HashMap.class;
					                    	}
						                    Class<?> genericClzz = (Class<?>) arr[1];
						                    val = fromJson(jsonArt, format, mapClz, genericClzz);
					                    }
						            } else {
					                    val = fromJson(jsonArt, format, field[i].getType());
						            }
								}
								setFieldValue(obj, field[i], val, format);
							} catch (Exception e) {
							}
						}
						objClz = objClz.getSuperclass();
					}
					return obj;
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("fromJson error : "+e.getMessage(), e);
			return null;
		}
	}
	
	public static JSONArtifact toJson(Object obj, FormatFactory formatFactory) {
		try {
			if(obj==null) return null;
			
			//format generic object
			try{
				JSONArray jsonArr = new JSONArray();
				jsonArr.add(objToString(obj, formatFactory));
				return jsonArr;
			} catch(Exception e){
			}
			
			if(obj instanceof List){
				List list = (List) obj;
				JSONArray jsonArr = new JSONArray();
				for(int i=0; i<list.size(); i++){
					try{
						jsonArr.add( objToString(list.get(i), formatFactory) );
					} catch(Exception e){
						jsonArr.add( toJson(list.get(i), formatFactory) );
					}
				}
				return jsonArr;
			} else if(obj instanceof Object[]){
				Object list[] = (Object[]) obj;
				JSONArray jsonArr = new JSONArray();
				for(int i=0; i<list.length; i++){
					try{
						jsonArr.add( objToString(list[i], formatFactory) );
					} catch(Exception e){
						jsonArr.add( toJson(list[i], formatFactory) );
					}
				}
				return jsonArr;
			} else if(obj instanceof Map){
				JSONObject jsonObj = new JSONObject();
				Map map = (Map) obj;
				Iterator itr = map.keySet().iterator();
				while(itr.hasNext()){
					Object key = itr.next();
					Object val = map.get(key);
					try{
						jsonObj.put(key.toString(), objToString(val, formatFactory));
					} catch(Exception e){
						jsonObj.put(key.toString(), toJson(val, formatFactory));
					}
				}
				return jsonObj;
			} else {
				JSONObject jsonObj = new JSONObject();
				Class clz = obj.getClass();
				while(!clz.equals(Object.class)){
					Field field[] = clz.getDeclaredFields();
					for (int i = 0; i < field.length; i++) {
						try {
							String fieldName = field[i].getName();
							Object val = getFieldValue(obj, field[i]);
							try{
								jsonObj.put(fieldName, objToString(val, formatFactory));
							} catch(Exception e){
								jsonObj.put(fieldName, toJson(val, formatFactory));
							}
						} catch (Exception e) {
						}
					}
					clz = clz.getSuperclass();
				}
				return jsonObj;
			}
		} catch (Exception e) {
			log.error("toJson error : "+e.getMessage(), e);
			return null;
		}
	}

	private static Object objToString(Object obj, FormatFactory formatFactory) throws Exception {
		if(obj==null){
			return null;
		} else if(obj instanceof String){
			return obj.toString();
		} else if(obj instanceof Number){
			Number num = (Number) obj;
			return NumberUtil.formatByType(num);
		} else if(obj instanceof Boolean){
			return obj;
		} else if(obj instanceof Date){
			return DateUtil.formatDate((Date) obj, formatFactory.getDateTimeFormat());
		} else {
			return obj;
		}
	}
	
	public static Object convertObjectToDefineType(Object obj, Class objClz, FormatFactory format) throws Exception {
		if(obj==null || objClz==null){
			return null;
		}
		
		//same type
		try{
			Class subClz = obj.getClass().asSubclass(objClz);
			if(subClz==null){
				return null;
			}
			return obj;
		} catch(Exception e){
		}
		
		//convert string
		try{
			Class subClz = objClz.asSubclass(String.class);
			if(subClz==null){
				return null;
			}
			return obj.toString();
		} catch(Exception e){
		}
		
		//convert common type
		if(objClz.toString().equalsIgnoreCase("int")){
			return NumberUtil.toInteger(obj);
		} else if(objClz.toString().equalsIgnoreCase("double")){
			return NumberUtil.toDouble(obj);
		} else if(objClz.toString().equalsIgnoreCase("float")){
			return NumberUtil.toBigDecimal(obj).floatValue();
		} else if(objClz.toString().equalsIgnoreCase("boolean")){
			return BooleanUtil.toBoolean(obj);
		}
		
		//convert date
		try{
			Class subClz = objClz.asSubclass(Date.class);
			if(subClz==null){
				return null;
			}
			String dteFormat = format.getDateTimeFormat();
			if(obj instanceof String){
				int len = obj.toString().length();
				if(len==format.getDateFormat().length()){
					dteFormat = format.getDateFormat();
				}
			}
			Date dte = DateUtil.toDate(obj, dteFormat, format.getLocale());
			if(objClz.getName().equalsIgnoreCase(Date.class.getName())){
				return dte;
			} else if(objClz.getName().equalsIgnoreCase(java.sql.Date.class.getName())){
				java.sql.Date sqlDate = new java.sql.Date(dte.getTime());
				return sqlDate;
			} else if(objClz.getName().equalsIgnoreCase(java.sql.Timestamp.class.getName())){
				java.sql.Timestamp sqlDate = new java.sql.Timestamp(dte.getTime());
				return sqlDate;
			}
			return dte;
		} catch(Exception e){
		}
		
		//convert number
		try{
			Class subClz = objClz.asSubclass(Number.class);
			if(subClz==null){
				return null;
			}
			return NumberUtil.toNumber(obj, objClz);
		} catch(Exception e){
		}

		//convert boolean
		try{
			Class subClz = objClz.asSubclass(Boolean.class);
			if(subClz==null){
				return null;
			}
			return BooleanUtil.toBoolean(obj);
		} catch(Exception e){
		}
		
		return obj;
	}
	
	private static Object stringToObj(String jsonStr, Class clz) throws Exception {
		if(clz==null){
			return null;
		}
		
		while(!clz.equals(Object.class)){
			if(clz.equals(String.class)){
				return jsonStr;
			} else if(clz.equals(Integer.class)){
				return NumberUtil.toInteger(jsonStr);
			} else if(clz.equals(Double.class)){
				return NumberUtil.toDouble(jsonStr);
			} else if(clz.equals(Number.class)){
				return NumberUtil.toBigDecimal(jsonStr);
			} else if(clz.equals(Boolean.class)){
				return BooleanUtil.toBoolean(jsonStr);
			} else if(clz.equals(Date.class)){
				return DateUtil.parseDate(jsonStr, DateUtil.defaultDateTimeFormat);
			}
			clz = clz.getSuperclass();
		}
		throw new Exception("stringToObj error : Unknow type for class "+clz.getClass().getName());
	}

	public static List sortList(List list, List<String> sortField, List<String> sortType){
		if(list==null || list.size()<2){
			return list;
		}
		if(sortField==null || sortField.size()==0){
			return list;
		}

		//get all field
		List<Field> fieldList = new ArrayList<Field>();
		try{
			Class clz = list.get(0).getClass();
			for(int i=0; i<sortField.size(); i++){
				Field field = clz.getDeclaredField(sortField.get(i));
				fieldList.add(field);
			}
		} catch(Exception e){
			log.error("sortList error while getField : "+e.getMessage(), e);
			return list;
		}
		
		try{
			for(int i=0; i<list.size(); i++){
				int focusIdx = i;
				Object focusObj = list.get(i);
				for(int j=i+1; j<list.size(); j++){
					Object obj = list.get(j);
					for(int k=0; k<fieldList.size(); k++){
						Object valJ = getFieldValue(obj, fieldList.get(k));
						Object valFocus = getFieldValue(focusObj, fieldList.get(k));
						if(valJ==null && valFocus==null){
							continue;
						}
						if(valJ==null){
							break;
						}
						if(valFocus==null){
							focusIdx = j;
							focusObj = obj;
							break;
						}
						
						String sortBy = "asc";
						try{
							sortBy = sortType.get(k);
						} catch(Exception e){
						}
						
						int compare = 0;
						try{
							Method compareMth = valJ.getClass().getMethod("compareTo", valJ.getClass());
							compare = (Integer) compareMth.invoke(valJ, valFocus);
						} catch(Exception e) {
							compare = valJ.toString().compareTo(valFocus.toString());
						}
						if(compare==0){
							continue;
						}
						
						if("asc".equalsIgnoreCase(sortBy) && compare<=-1){
							focusIdx = j;
							focusObj = obj;
							break;
						} else if("desc".equalsIgnoreCase(sortBy) && compare>=1){
							focusIdx = j;
							focusObj = obj;
							break;
						} else {
							break;
						}
					}
				}
				
				if(i!=focusIdx){
					log.debug("swap = "+i+", focusIdx = "+focusIdx);
					Object iObj = list.get(i);
					focusObj = list.get(focusIdx);
					list.set(i, focusObj);
					list.set(focusIdx, iObj);
				}
			}
		} catch(Exception e){
			log.error("sortList error : "+e.getMessage(), e);
		}
		return list;
	}
	
	public static Object getFieldValue(Object obj, Field field) throws Exception {
		String fieldName = field.getName();
		String objType = field.getType().getName();
		Method mth = null;
		if("boolean".equalsIgnoreCase(objType) || Boolean.class.getName().equalsIgnoreCase(objType) ){
			if(fieldName.indexOf("is")==0){
				mth = obj.getClass().getMethod(fieldName, null);
			} else {
				String mthName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
				mth = obj.getClass().getMethod("is"+mthName, null);
			}
		} else {
			String mthName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
			mth = obj.getClass().getMethod("get" + mthName, null);
		}
		Object val = mth.invoke(obj, null);
		return val;
	}

	public static void setField(Object obj, String field, Object value, FormatFactory format){
		if(obj==null || field==null || value==null || format==null){
			return;
		}
		try {
			Field f = obj.getClass().getDeclaredField(field);
			setFieldValue(obj, f, value, format);
		} catch (Exception e) {
			log.error("setField error : "+e.getMessage(), e);
		}
	}
	
	public static void setFieldValue(Object obj, Field field, Object val, FormatFactory format) throws Exception {
		String fieldName = field.getName();
		String objType = field.getType().getName();
		Method mth = null;
		if("boolean".equalsIgnoreCase(objType) || Boolean.class.getName().equalsIgnoreCase(objType) ){
			String mthName = fieldName;
			if(fieldName.indexOf("is")==0){
				mthName = fieldName.substring(2);
			}
			mthName = mthName.substring(0, 1).toUpperCase() + mthName.substring(1);
			mth = obj.getClass().getMethod("set" + mthName, field.getType());
		} else {
			String mthName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
			mth = obj.getClass().getMethod("set" + mthName, field.getType());
		}
		Object convertVal = convertObjectToDefineType(val, field.getType(), format);
		if(convertVal==null){
			return;
		}
		mth.invoke(obj, convertVal);
	}
}
