package test.icp.util;

import org.apache.log4j.Logger;

import java.util.Locale;

public class FormatFactory {
	private static final Logger log = Logger.getLogger(FormatFactory.class);
	public static final FormatFactory defaultFormat;
	public static final FormatFactory igmFormat;
	public static final FormatFactory esbAiaServiceFormat;
	public static final FormatFactory restProviderFormat;
	public static final FormatFactory restProviderEsbFormat;

	private Locale locale;
	private String dateFormat;
	private String dateTimeFormat;
	
	static {
		defaultFormat = new FormatFactory(DateUtil.defaultDateFormat, DateUtil.defaultDateTimeFormat, DateUtil.defaultLocale);
		igmFormat = new FormatFactory("yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss.S", DateUtil.defaultLocale);
		esbAiaServiceFormat = new FormatFactory("yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss.S", DateUtil.defaultLocale);
		restProviderFormat = new FormatFactory("yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", DateUtil.defaultLocale);
		restProviderEsbFormat = new FormatFactory("yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss.S", DateUtil.defaultLocale);
	}
	
	public FormatFactory(
			String dateFormat, String dateTimeFormat, Locale locale
		){

		this.dateFormat = dateFormat;
		this.dateTimeFormat = dateTimeFormat;
		this.locale = locale;
	}
	
	public String getDateFormat() {
		return dateFormat;
	}
	public String getDateTimeFormat() {
		return dateTimeFormat;
	}

	public Locale getLocale() {
		return locale;
	}
}
