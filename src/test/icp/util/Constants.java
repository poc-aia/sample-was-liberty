package test.icp.util;

import java.net.InetAddress;

import org.apache.log4j.Logger;

public class Constants {
	private static final Logger log = Logger.getLogger(Constants.class);
	public static final String serverName;

	//ESB
	public static final String esbReturnSuccess = "S";
	public static final String esbReturnError = "E";
	
	//Rest
	public static final String restReturnSuccessCode = "S";
	public static final String restReturnErrorCode = "E";
	public static final String restReturnProcessingCode = "P";
	
	static {
		log.info("init : serverName");
		String sName = null;
		try{
			sName = InetAddress.getLocalHost().getHostName();
		} catch(Exception e){
			log.error("get sName error : "+e.getMessage());
		}
		if(StringUtil.isBlank(sName)){
			sName = "iService";
		}
		serverName = sName;
		log.info("end : serverName = "+serverName);
	}
}
