package test.icp.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.log4j.Logger;

public class StringUtil {
	private static final Logger log = Logger.getLogger(StringUtil.class);

	public static final String trim(String str) throws Exception {
		if (isBlank(str)) return str;
		return str.trim();
	}

	public static String setEncode(String str, String enc, String dec){
		if(str==null) return str;
		
		try{
			if(dec==null){
				return new String(str.getBytes(), enc);
			} else {
				return new String(str.getBytes(dec), enc);
			}
		} catch (Exception e) {
			return str;
		}
	}
	
	public static String blankWhenNull(Object obj){
		if(obj==null) return "";
		else return obj.toString().trim();
	}

	public static boolean isBlank(String str){
		if(str==null) return true;
		if(str.replace(" ", "").trim().equalsIgnoreCase("")){
			return true;
		}
		return false;
	}

	public static final String encodeURIComponent(Object obj) {
		if (obj == null)
			return null;

		try {
			ScriptEngineManager factory = new ScriptEngineManager();
			ScriptEngine engine = factory.getEngineByName("JavaScript");
			Object result = engine.eval("encodeURIComponent('" + obj.toString().replace("'", "\\'") + "')");
			return result == null ? obj.toString() : result.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return obj.toString();
		}
	}
}
