package test.icp.service;

import test.icp.model.InputModel;
import test.icp.model.ResultModel;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;


@WebService (targetNamespace="http://service.icp.test/", serviceName="TestSoapService", portName="TestSoapPort", wsdlLocation="WEB-INF/wsdl/TestSoapService.wsdl")
@BindingType (value=SOAPBinding.SOAP12HTTP_BINDING)
public class TestSoapDelegate{

    test.icp.service.TestSoap _testSoap = null;

    public ResultModel sayHello (InputModel input) {
        return _testSoap.sayHello(input);
    }

    public TestSoapDelegate() {
        _testSoap = new test.icp.service.TestSoap(); }

}