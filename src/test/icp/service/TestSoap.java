package test.icp.service;

import org.apache.log4j.Logger;

import test.icp.model.InputModel;
import test.icp.model.ResultModel;
import test.icp.servlet.RestServlet;
import test.icp.util.FormatFactory;
import test.icp.util.HttpLogUtil;
import test.icp.util.ObjectUtil;

public class TestSoap {
	private static final Logger log = Logger.getLogger(TestSoap.class);

	public ResultModel sayHello(InputModel input){
		ResultModel result = new ResultModel();
		if(input==null){
			result.setMessage("input is null");
		} else if(input.getName()==null){
			result.setMessage("input.name is null");
		} else if(input.getName().trim().equalsIgnoreCase("")){
			result.setMessage("input.name is blank");
		} else {
			result.setMessage("Hello : "+input.getName());
		}
		
		try {
			String logMsg = ObjectUtil.toJson(result, FormatFactory.restProviderFormat).serialize();
			HttpLogUtil.log( logMsg );
			log.info( logMsg );
		} catch (Exception e) {
			log.error("error : "+e.getMessage(), e);
		}
		return result;
	}
}
