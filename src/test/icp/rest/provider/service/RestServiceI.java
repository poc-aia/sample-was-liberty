package test.icp.rest.provider.service;

import test.icp.util.FormatFactory;

public interface RestServiceI {
	public FormatFactory getFormatFactor();
	public String getServiceCode();
	public Class getInputClass();
	public Object execute(Object input) throws Exception;
}
