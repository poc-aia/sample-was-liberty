package test.icp.rest.provider.service.test;

import org.apache.log4j.Logger;

import test.icp.rest.provider.model.io.TestIcpInputModel;
import test.icp.rest.provider.model.io.TestIcpResultModel;
import test.icp.rest.provider.service.RestServiceI;
import test.icp.util.FormatFactory;

public class TestIcp implements RestServiceI {
	private static final Logger log = Logger.getLogger(TestIcp.class);
    
	public String getServiceCode() {
		return "TEST-001";
	}

	public Class getInputClass() {
		return TestIcpInputModel.class;
	}

	public TestIcpResultModel execute(Object inputObj) throws Exception {
		try {
			TestIcpInputModel input = (TestIcpInputModel) inputObj;
			TestIcpResultModel result = new TestIcpResultModel();
			result.setMessage("Hello Rest, I'm "+input.getName());
			return result;
		} catch (Exception e) {
			log.error("error : "+e.getMessage(), e);
			throw e;
		}
	}
	
	public FormatFactory getFormatFactor() {
		return FormatFactory.restProviderEsbFormat;
	}
	
}
