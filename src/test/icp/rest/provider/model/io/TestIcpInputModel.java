package test.icp.rest.provider.model.io;

import test.icp.rest.provider.model.RestInputModel;

public class TestIcpInputModel extends RestInputModel {
	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
