package test.icp.rest.provider.model.io;

public class TestIcpResultModel {
	private String message;

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
