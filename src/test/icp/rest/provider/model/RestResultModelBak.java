package test.icp.rest.provider.model;

import java.math.BigInteger;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import test.icp.rest.provider.service.RestServiceI;
import test.icp.util.Constants;

public class RestResultModelBak {
	private static final String app = "TestICP";
	private String returnCode;
	private String returnMessage;
	private String trxCode;
	private String inputJson;
	private String requestUri;
	private String serviceCode;
	private String javaModule;
	private String requesterIp;
	private String requesterHost;
	private Date requestDate;
	private BigInteger startTime;
	private BigInteger endTime;
	private BigInteger processTime;
	private Object result;
	
	public RestResultModelBak(){
		Date currentTime = new Date(System.currentTimeMillis());
		
		this.returnCode = "P";
		this.returnMessage = "Processing";
		this.trxCode = Long.toHexString(System.currentTimeMillis())+"@"+Constants.serverName;
		this.requestDate = currentTime;
		this.startTime = BigInteger.valueOf(currentTime.getTime());
	}

	public void setupServiceInfo(RestServiceI restService, String inputJson, HttpServletRequest request) {
		if(request!=null){
			this.requestUri = request.getRequestURI();
			this.requesterIp = request.getRemoteAddr();
			this.requesterHost = "-";
		}
		this.inputJson = inputJson;
		this.serviceCode = restService.getServiceCode();
		this.javaModule = restService.getClass().getName();
	}
	
	public void responseSuccess(Object result) {
		this.endTime = BigInteger.valueOf(System.currentTimeMillis());
		this.processTime = this.endTime.subtract(this.startTime);
		this.result = result;
		this.returnCode = Constants.restReturnSuccessCode;
		this.returnMessage = "Success";
	}
	
	public void responseError(Exception e) {
		this.endTime = BigInteger.valueOf(System.currentTimeMillis());
		this.processTime = this.endTime.subtract(this.startTime);
		this.result = null;
		this.returnCode = Constants.restReturnErrorCode;
		this.returnMessage = e.getMessage();
	}
	
	public String getReturnCode() {
		return returnCode;
	}
	public String getReturnMessage() {
		return returnMessage;
	}
	public Object getResult() {
		return result;
	}
	public String getInputJson() {
		return inputJson;
	}
	public String getRequestUri() {
		return requestUri;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public String getJavaModule() {
		return javaModule;
	}
	public String getTrxCode() {
		return trxCode;
	}
	public String getRequesterIp() {
		return requesterIp;
	}
	public String getRequesterHost() {
		return requesterHost;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public BigInteger getStartTime() {
		return startTime;
	}
	public BigInteger getEndTime() {
		return endTime;
	}
	public BigInteger getProcessTime() {
		return processTime;
	}
	public static String getApp() {
		return app;
	}
	
}
