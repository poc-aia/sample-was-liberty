package test.logger;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import test.icp.rest.provider.model.RestResultModel;
import test.icp.util.FormatFactory;
import test.icp.util.ObjectUtil;


public class Test {
	private static final Logger log = Logger.getLogger(Test.class);
	private static final org.slf4j.Logger slf4jlog = LoggerFactory.getLogger(Test.class);

	
	public static void main(String[] args) {
		try {
			log.info("{\"app\":\"TestICP\"}");
			slf4jlog.info("{\"app\":\"TestICP1\"}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
