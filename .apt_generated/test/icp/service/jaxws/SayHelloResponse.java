
package test.icp.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "sayHelloResponse", namespace = "http://service.icp.test/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sayHelloResponse", namespace = "http://service.icp.test/")
public class SayHelloResponse {

    @XmlElement(name = "return", namespace = "")
    private test.icp.model.ResultModel _return;

    /**
     * 
     * @return
     *     returns ResultModel
     */
    public test.icp.model.ResultModel getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(test.icp.model.ResultModel _return) {
        this._return = _return;
    }

}
