
package test.icp.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "sayHello", namespace = "http://service.icp.test/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sayHello", namespace = "http://service.icp.test/")
public class SayHello {

    @XmlElement(name = "arg0", namespace = "")
    private test.icp.model.InputModel arg0;

    /**
     * 
     * @return
     *     returns InputModel
     */
    public test.icp.model.InputModel getArg0() {
        return this.arg0;
    }

    /**
     * 
     * @param arg0
     *     the value for the arg0 property
     */
    public void setArg0(test.icp.model.InputModel arg0) {
        this.arg0 = arg0;
    }

}
